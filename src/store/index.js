import { createStore } from 'vuex'

export default createStore({
  state: {
    usersStore: [],
    usersFavorites: [],
    profileType: 'normal'
  },
  mutations: {
    addUserStore (state, payload) {
      state.usersStore = payload
    },
    addUsersFavoriteStore (state, payload) {
      let checkUser = state.usersFavorites.find(item => item.email == payload.email)
      if(checkUser === undefined) {
        state.usersFavorites.push(payload)
      } else {
        state.usersFavorites.forEach((item, index) => {
          if (item.email == payload.email) {
            state.usersFavorites.splice(index, 1);
          }
        })
      }
    },
    changeProfileTypeStore (state, payload) {
      state.profileType = payload
    },
  },
  actions: {
  },
  modules: {
  }
})
